import scipy as sp
from sklearn import cluster, datasets
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


try:
    face = sp.face(gray=True)
except AttributeError:
    from scipy import misc
    face = misc.face(gray=True)   

X = face.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=5,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
face_compressed = np.choose(labels, values)
face_compressed.shape = face.shape

plt.figure(1)
plt.imshow(face,  cmap='gray')

plt.figure(2)
plt.imshow(face_compressed,  cmap='gray')

image = mpimg.imread('../resources/example_grayscale.png') 
Slika = image.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means2 = cluster.KMeans(n_clusters=5,n_init=1)
k_means2.fit(Slika)

values = k_means2.cluster_centers_.squeeze()
labels = k_means2.labels_
Slika_compressed = np.choose(labels, values)
Slika_compressed.shape = image.shape
print(Slika_compressed)

plt.figure(3)
plt.imshow(image,  cmap='gray')

plt.figure(4)
plt.imshow(Slika_compressed,  cmap='gray')
