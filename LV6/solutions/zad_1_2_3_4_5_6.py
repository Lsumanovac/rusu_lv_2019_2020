import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as met

def generate_data(n):
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1)); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1))
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2)
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

##################################################################
#Prvi zadatak
np.random.seed(242)
train_data=generate_data(200)

print(train_data)
np.random.seed(12)

test_data=generate_data(100)  
  
x1=train_data[:,0]
x2=train_data[:,1]

Y=train_data[:,2]

##################################################################
#Drugi zadatak
plt.scatter(x=x1,y=x2,c=Y)

##################################################################
#Treci zadatak
log=lm.LogisticRegression(solver='liblinear')
log.fit(train_data[:,:2],train_data[:,2])

ax = plt.gca()
xs = np.array(ax.get_xlim())
ys = -(xs * log.coef_[0][0] + log.intercept_[0])/log.coef_[0][1]
plt.plot(xs, ys)

##################################################################
#Četvrti zadatak
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(train_data[:,0])-0.5:max(train_data[:,0])+0.5:.05,
                          min(train_data[:,1])-0.5:max(train_data[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = log.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)

ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
ax.autoscale(False)
plt.scatter(train_data[:, 0], train_data[:, 1], c=train_data[:, 2])
plt.show()

##################################################################
#Peti zadatak
predicted =log.predict(test_data[:,:2])
test_e=test_data[:,2]
colors = ['g' if predicted[i] == test_e[i] else 'k' 
               for i in range(len(predicted))]

plt.scatter(x=test_data[:,0],y=test_data[:,1],c=colors)

##################################################################
#Sesti zadatak
confusion_matrix = met.confusion_matrix(test_e, predicted)
plot_confusion_matrix(confusion_matrix)
print(confusion_matrix)
print('Accuracy = %.2f' %met.accuracy_score(test_data[:,2], predicted))
print('Missclassification rate = %.2f' %(1-met.accuracy_score(test_data[:,2], predicted)))
print('Precision = %.2f' %met.precision_score(test_data[:,2], predicted))
print('Recall = %.2f' %met.recall_score(test_data[:,2], predicted))
tn, fp, fn, tp = met.confusion_matrix(test_data[:,2], predicted).ravel()
specificity = tn / (tn+fp)
print('Specificity = %.2f' %specificity)
