lista = []

file_name = input('Unesite ime filea: ')  
try:
    fhand = open(file_name)
except:
    print ('File cannot be opened:', file_name)
    exit()

for line in fhand:
    if(line.startswith("X-DSPAM-Confidence:")):
        linePart=line.split()
        lista.append(float(linePart[1]))
       
print("Ime datoteke",file_name)
print("Average X-DSPAM-Confidence:",sum(lista)/len(lista))
