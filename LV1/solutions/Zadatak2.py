ocjena = float(input("Unesite broj koji predstavlja nekakvu ocjenu, broj se treba nalaziti izmedu 0.0 i 1.0: "))
if ocjena < 0.6 and ocjena > 0:
    oznaka = 'F'
elif ocjena >= 0.6 and ocjena < 0.7:
    oznaka = 'D'
elif ocjena >= 0.7 and ocjena < 0.8:
    oznaka = 'C'
elif ocjena >= 0.8 and ocjena < 0.9:
    oznaka = 'B'
elif ocjena >= 0.9 and ocjena < 1.0:
    oznaka = 'A'

try:
    print(oznaka)
except:
    print("Niste unijeli ocjenu u zadanom intervalu.")