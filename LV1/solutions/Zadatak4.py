import math

brojac = 0
suma = 0
flag = 0
minimalna_vrijednost = 0
maksimalna_virjednost = 0

while(1):
    novi_unos = input("Unesite broj, ili \"Done\", bez navodnika: ")

    if novi_unos == "Done":
        print("Srednja vrijednost unesenih brojeva: ", suma/brojac)
        print("Minimalna vrijednost: ", minimalna_vrijednost)
        print("Maksimalna vrijednost: ", maksimalna_virjednost)
        print("Korisnik je ukupno unio ", brojac, " brojeva")
        exit()
    elif math.isnan(int(novi_unos)):
        print("Krivi unos.")
    else:
        if flag == 0:
            flag = 1
            minimalna_vrijednost = int(novi_unos)
            maksimalna_virjednost = int(novi_unos)
        brojac = brojac + 1
        suma = suma + int(novi_unos)
        if int(novi_unos) > maksimalna_virjednost:
            maksimalna_virjednost = int(novi_unos)
        elif int(novi_unos) < minimalna_vrijednost:
            minimalna_vrijednost = int(novi_unos)
        