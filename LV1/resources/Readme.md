Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Izmjene na counting_words.py:
1. raw_input -> input
2. zagrade kod print poruke
3. fnamex -> fname
4. zagrade kod print counts
